<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Backs in Action</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/mobile.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="http://malsup.github.com/jquery.cycle2.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	
	<div class="mc-container container">
	
<div id="header" class="row">
	<div class="header-inner">
		<a href="#"><img src="images/banner-logo.png" alt="Logo" title="Logo" class="logo img-responsive pull-left"></a>
		<div class="appointment pull-right">
			<div class="app-label slant "><div>Call For Appointment</div></div>
			<div class="breaker hidden-xs hidden-md hidden-lg"><br><br></div>
			<div class="app-number slant "><div>778.783.3838</div></div>
		</div>
	</div>
	

	<div class="row menu-top-container">
		<div id="menu">
			<div class="menu-links col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-sm-11 col-md-11 col-lg-11">
				<nav class="navbar navbar-default helv">
				  <div class="container-fluid header-menu">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					  <button id="mobile-menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						MENU
					  </button>
					  <!-- <a href="#"><img src="images/banner-logo.png" alt="Logo" title="Logo" class="logo hidden-sm hidden-lg hidden-md pull-left" width=137 style="margin-left:20px;"></a> -->
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav">
						<li  class="active"><a href="#">Home</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> About</a>
							<ul class="dropdown-menu multi-level" role="menu">
							  <li><a href="#">About Us</a></li>
							<li class="dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Our Team</a>
								<ul class="dropdown-menu">
									<li><a href="#">Farrah Jiwa</a></li>
									<li><a href="#">Farrah Jiwa</a></li>
								</ul>
							</li>	
								
							</ul>
						</li>
						<li><a href="#">Chiropractic</a></li>
						<li><a href="#">Custom Orthotics</a></li>
						<li><a href="#">Massage</a></li>
						<li><a href="#">Other Services</a></li>
						<li><a href="#">FAQs</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact</a></li>
					  </ul>
					</div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
			</div>
		</div>
	</div>


	<div class="row">
		<div id="banner" class="col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xs-10 col-sm-10 col-md-10 col-lg-10">
			<div id="banner-slide" class="cycle-slideshow">
				<img src="images/banner1.png" alt="Banner" title="Banner 1" class="img-responsive">
				<img src="images/banner2.png" alt="Banner" title="Banner 2" class="img-responsive">
				<img src="images/banner3.png" alt="Banner" title="Banner 3" class="img-responsive">
				<img src="images/banner4.png" alt="Banner" title="Banner 4" class="img-responsive">
			</div>
		</div>
	</div>

	<div class="appointment bottom">
		<div class="app-label slant "><div>Call For Appointment</div></div>
		<div class="breaker hidden-xs hidden-md hidden-lg"><br><br></div>
		<div class="app-number slant "><div>778.783.3838</div></div>
	</div>

	<div class="row menu-bottom-container">
		<div id="menu">
			<div class="menu-links col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-sm-11 col-md-11 col-lg-11">
				<nav class="navbar navbar-default helv">
				  <div class="container-fluid header-menu">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					  <button id="mobile-menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
						<span class="sr-only">Toggle navigation</span>
						MENU
					  </button>
					  <!-- <a href="#"><img src="images/banner-logo.png" alt="Logo" title="Logo" class="logo hidden-sm hidden-lg hidden-md pull-left" width=137 style="margin-left:20px;"></a> -->
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
					  <ul class="nav navbar-nav">
						<li  class="active"><a href="#">Home</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> About</a>
							<ul class="dropdown-menu multi-level" role="menu">
							  <li><a href="#">About Us</a></li>
							<li class="dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Our Team</a>
								<ul class="dropdown-menu">
									<li><a href="#">Farrah Jiwa</a></li>
									<li><a href="#">Farrah Jiwa</a></li>
								</ul>
							</li>	
								
							</ul>
						</li>
						<li><a href="#">Chiropractic</a></li>
						<li><a href="#">Custom Orthotics</a></li>
						<li><a href="#">Massage</a></li>
						<li><a href="#">Other Services</a></li>
						<li><a href="#">FAQs</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact</a></li>
					  </ul>
					</div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
			</div>
		</div>
	</div>
</div>
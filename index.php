<?php include('header.php'); ?>


<div class="row content-main">   
	<div class="wrapper">
	<div id="sidebar" class="">
		<div class="bk-app"><h2>Book Your Appointment!</h2></div> <!-- bk-app -->

		<div class="left-box">
			<div class="box-title">
				<div class="clk-label"><a href="#">CLICK HERE</a></div>
				<img src="images/clock.png" class="pull-right img-responsive">
			</div>
			<div class="left-box-content">
				<h2>
					CONVENIENTLY <br>LOCATED
				</h2>
				<div class="helv cc-broad">
					Close to Commercial - <br>
					Broadway Skytrain Station
				</div>
				<h4>
					Suite 203, 1750 E. 10th Ave<br />Vancouver, BC<br><br>
					T: 604.876.9977
				</h4>
			</div>
			<img src="images/map.jpg" width=100%>
		</div> <!-- left-box -->
		
		<div class="left-box2">
			<div class="box-title">
				<h2>Our Blog</h2>
			</div>
			<h4>6 APR, 2012</h4>
			Lorem Ipsum is simply dummy<br>
				text of the printing and types...<br><br>
			<h4>6 APR, 2012</h4>
			Lorem Ipsum is simply dummy<br>
				text of the printing and types...<br><br>
			<div class="read-more"><div class="readmore-border"></div><a href="#">READ MORE</a></div>
		</div> <!-- left-box2 -->
		
		<div class="freq-ask">
			<h4>FREQUENTLY ASKED<br>QUESTIONS</h4>
			<img src="images/question-mark.png" class="pull-right">
			<div class="read-more"><div class="readmore-border"></div><a href="#">CLICK HERE</a></div>
		</div>
		
	</div>
	<div id="content" class="">
		<div class="row content-holder">
			<h2 class="content-title"><b>BACKS IN ACTION</b></h2>
			<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, 
				condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, 
				bibendum at, posuere sit amet, nibh. Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate 
				aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat 
				rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, 
				imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus 
				sollicitudin metus eget eros. </p><br />

				<b class="bigger-text sec">MAURIS VEL LACUS VITAE FELIS VESTIBULUM</b>
				<img src="images/sample.jpg" clas="img-responsive" style="float:right; margin-left: 35px; margin-bottom: 20px; "><br />
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, 
					condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, 
					bibendum at, posuere sit amet, nibh. Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate 
					aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat 
					rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, 
					imperdiet vitae, tellus. Sed odio est,<br /><br />
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, 
					condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, 
					bibendum at, posuere sit amet, nibh. Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate 
					aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat 
					rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, 
					imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus 
					sollicitudin metus eget eros.
			</div>
		</div>
		
			<div class="row content-holder">
			<h2 class="content-title2">HAPPY CLIENTS</h2>
			<div class="pg col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<img src="images/left-quote.png" id="prev">
			</div>
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 cycle-slideshow hc-slide"
				data-cycle-fx="scrollHorz" 
				data-cycle-timeout="0"
				data-cycle-prev="#prev"
				data-cycle-next="#next"
				data-cycle-slides="> div"
			>
			<div class="quote">
				<div class="quote-intro">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, 
					condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, 
					bibendum at...<a href="#" style="color:#ec4d1a;">James Dawson</a>
				</div>
				<footer class="read-more c2-rm pull-right">
					<div class="readmore-border quote-border"></div><a href="#">READ MORE</a>
				</footer>
			</div>
			<div class="quote">
				<div class="quote-intro">
					2Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, 
				condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, 
				bibendum at...<a href="#" style="color:#ec4d1a;">James Dawson</a>
				</div>

				<footer class="read-more c2-rm pull-right">
					<div class="readmore-border quote-border"></div><a href="#">READ MORE</a>
				</footer>
			</div>	

			</div>
			
			<div class="pg  col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<img src="images/right-quote.png"  id="next">
			</div>
			
		</div>
		
		
	</div>
</div>	
</div>
<?php include('footer.php'); ?>

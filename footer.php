<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="footer-box1 slant">
					<div class="footer-box-address helv">
						<address>
							#230 - 3433 North Road<br>
							Burnaby, BC V3J OA9
						</address>
					</div>
				</div>
				<div class="footer-box2 slant">
					<ul class="footer-nav helv ">
								<li><a href="#">Home</a></li>
								<li><a href="#">About</a></li>
								<li><a href="#">Chiropractic</a></li>
								<li><a href="#">Custom Orthotics</a></li>
								<li><a href="#">Massage</a></li>
								<li><a href="#">Other Services</a></li>
								<li><a href="#">FAQs</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Contact</a></li>
							  </ul>
					<img src="images/footer-logo.png" class="pull-right footer-logo hidden-xs hidden-sm">		  
				</div>
			</div>
		</div>
		<div class="row helv footer-text">
			<div class="address">
				<div class="ft1">
				#230 - 3433 North Road<br>
				Burnaby, BC V3J OA9
				</div>
				<div class="ft2">
				Phone: 778.783.3838<br>
				E-mail: info@backsinaction.com
				</div>
			</div>
			
			<div class="copyright">
				&copy; Copyright 2013 Backs in Action | All Rights Reserved<br>
				<span class="pbi">Powered by IvoryShore</span> 
			</div>

			<div class="footer-icons col-md-offset-4 col-lg-offset-4 col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<a href="#"><img src="images/fb-icon.jpg"></a>
				<a href="#"><img src="images/in-icon.jpg"></a>
				<a href="#"><img src="images/gplus-icon.jpg"></a>
				<a href="#"><img src="images/yt-icon.jpg"></a>
			</div>
		</div> <!-- footer-text -->
	</div>
	<div id="fixed-box">
		<div class="rotate"><a href="#">Book an Appointment</a></div>
	</div>



	    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	 <script src="js/script.js"></script>
  </body>
</html>